Pullus Display Management 10.12.2022
====================================

A Swift framwork and command line tool to change the brightness of a display.


Command line tool
-----------------

`pudima` (**P**ull**u**s **Di**splay **Ma**nager) is a command line tool to inspect and change the connected displays.


### Example: Displays

Show all devices:

```
pudima list
```

### Example: Brightness

Change the brightness of a specifc display to 50%:

```
pudima display 4A21EBB6-9D30-6A3A-F1E8-D4FBFBA06B3E brightness 50
```

### Help

```
NAME
    pudima - Pullus Display Manager

SYNOPSIS
    Manage connected (Apple) displays.

    help
        Shows this text.

    main
        Shows the main display.

    list
        Shows all displays.

    brightness set <level>
        Sets the brightness of all displays to a level (0 to 100).

    brightness change <delta>
        Changes the brightness of all displays by a delta (-100 to 100).

    display <uuid> brightness set <level>
        Sets the brightness of a specific display (hex number) to a level (0 to 100).

    display <uuid> brightness change <delta>
        Changes the brightness of a specific display (UUID) by a delta (-100 to 100).

EXAMPLES
    pudima brightness set 100

    Sets all displays to the maximum brightness.

AUTHOR
    Frank Schuster
```


Restrictions
------------

 - Supports only Apple displays.
 - Needs a private Apple framework


Requirements
------------

Developed and tested with MacOS 10.12.

This software was intentionally developed for macOS 10.12 to be able to continue using older devices.
