import CoreGraphics

public
extension CGDirectDisplayID {
	
	///
	/// Convenience interface to the **private** Apple `DisplayServices`
	/// framework
	///
	/// See: `DisplayServicesSetBrightness`
	///
	@discardableResult
	public
	func setBrightness(to aLevel: Float) -> Bool {
		let level = aLevel.atLeast(0.0).atMost(1.0)
		
		return DisplayServicesSetBrightness(self, level) == kIOReturnSuccess
	}
	
}
