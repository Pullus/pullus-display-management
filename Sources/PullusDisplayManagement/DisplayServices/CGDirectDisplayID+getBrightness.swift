import CoreGraphics

public
extension CGDirectDisplayID {
	
	///
	/// Convenience interface to the **private** Apple `DisplayServices`
	/// framework
	///
	/// See: `DisplayServicesGetBrightness`
	///
	public
	func getBrightness() -> Float? {
		var level: Float = 0
		
		guard DisplayServicesGetBrightness(self, &level) == 0 else { return nil }
		
		return level
	}
	
}
