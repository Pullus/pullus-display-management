import Foundation

import PullusBase

public
extension CGDirectDisplayID {
	
	///
	/// Returns the UUID of the display.
	///
	/// The UUID is unique even if the display uses multiple GPU's.
	///
	public
	var UUID: UUID { return CGDisplayCreateUUIDFromDisplayID(self).takeRetainedValue().UUID }
	
}
