import CoreGraphics

public
extension CGDirectDisplayID {
	
	///
	/// Convenience interface
	///
	/// See: `CGDisplayIsMain`
	///
	public
	var isMain: Bool {
		return CGDisplayIsMain(self) != 0
	}
	
	///
	/// Convenience interface. See `CGDisplayIsActive`.
	///
	public
	var isActive: Bool {
		return CGDisplayIsActive(self) != 0
	}
	
	///
	/// Convenience interface. See `CGDisplayIsAsleep`.
	///
	public
	var isAsleep: Bool {
		return CGDisplayIsAsleep(self) != 0
	}
	
	///
	/// Convenience interface. See `CGDisplayIsOnline`.
	///
	public
	var isOnline: Bool {
		return CGDisplayIsOnline(self) != 0
	}
	
	///
	/// Convenience interface. See `CGDisplayIsBuiltin`.
	///
	public
	var isBuiltin: Bool {
		return CGDisplayIsBuiltin(self) != 0
	}
	
	///
	/// Convenience interface. See `CGDisplayIsStereo`.
	///
	public
	var isStereo: Bool {
		return CGDisplayIsStereo(self) != 0
	}
	
}
