import CoreGraphics

public
extension CGDirectDisplayID {
	
	///
	/// Convenience interface
	///
	/// See: `CGDisplayVendorNumber`
	///
	public
	var vendorNumber: UInt32? {
		let result = CGDisplayVendorNumber(self)
		
		switch result {
		case UInt32(kDisplayVendorIDUnknown):	return nil
		case 0xFFFFFFFF:	return nil
		default:			return result
		}
	}
	
}
