import CoreGraphics

public
extension CGDirectDisplayID {
	
	///
	/// Convenience interface
	///
	/// See: `CGDisplayBounds`
	///
	public
	var resolution: CGSize {
		return CGDisplayBounds(self).size
	}
	
}
