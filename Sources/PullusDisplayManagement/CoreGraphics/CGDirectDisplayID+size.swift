import CoreGraphics

public
extension CGDirectDisplayID {
	
	///
	/// Convenience interface
	///
	/// See: `CGDisplayScreenSize`
	///
	public
	var size: CGSize {
		return CGDisplayScreenSize(self)
	}
	
}
