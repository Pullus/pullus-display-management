import CoreGraphics

public
extension CGDirectDisplayID {
	
	///
	/// Convenience interface
	///
	/// See: `CGDisplaySerialNumber`
	///
	public
	var serialNumber: UInt32? {
		let result = CGDisplaySerialNumber(self)
		
		switch result {
		case 0x00000000:	return nil
		case 0xFFFFFFFF:	return nil
		default:			return result
		}
	}
	
}
