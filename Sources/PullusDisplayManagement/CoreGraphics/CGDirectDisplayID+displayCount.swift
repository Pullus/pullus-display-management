import Foundation
import CoreGraphics

public
extension CGDirectDisplayID {
	
	///
	/// Convenience interface
	///
	/// See: `CGGetOnlineDisplayList`
	///
	public
	static var displayCount: UInt32 {
		var count: UInt32 = 0
		
		guard CGGetOnlineDisplayList(0, nil, &count) == .success else { return 0 }
		
		return count
	}
	
}
