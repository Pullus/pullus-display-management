import Foundation
import CoreGraphics

public
extension CGDirectDisplayID {
	
	///
	/// Convenience interface
	///
	/// See: `CGGetOnlineDisplayList`
	///
	public
	static var displays: [CGDirectDisplayID] {
		let count = displayCount
		
		// An array to fetch the ids of all displays
		var displayIDs = [CGDirectDisplayID](repeating: 0, count: Int(count))
		
		// A variable to store the number of fetched ids
		var resultCount: UInt32 = 0
		
		guard CGGetOnlineDisplayList(count, &displayIDs, &resultCount) == .success else { return [] }
		guard resultCount == count else { return [] }
		
		return displayIDs
	}
	
}
