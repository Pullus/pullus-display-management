import CoreGraphics

public
extension CGDirectDisplayID {
	
	///
	/// Convenience interface
	///
	/// See: `CGDisplayCopyDisplayMode`
	///
	public
	var mode: CGDisplayMode? {
		return CGDisplayCopyDisplayMode(self)
	}
	
}
