// Used by the "framework" target

#import <Cocoa/Cocoa.h>

//! Project version number for PullusDisplayManagement.
FOUNDATION_EXPORT double PullusDisplayManagementVersionNumber;

//! Project version string for PullusDisplayManagement.
FOUNDATION_EXPORT const unsigned char PullusDisplayManagementVersionString[];

// In this header, you should import all the public headers of your framework
// using statements like #import <PullusDisplayManagement/PublicHeader.h>

#import <PullusDisplayManagement/DisplayServices.h>
