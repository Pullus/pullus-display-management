#ifndef _DISPLAY_SERVICES_H_
#define _DISPLAY_SERVICES_H_

///
/// The missing prototypes of the **private** Apple `DisplayService` framework.
///

#include <CoreGraphics/CGDirectDisplay.h>

extern void
DisplayServicesBrightnessChanged(CGDirectDisplayID id, double level)
__attribute__((weak_import));

extern bool
DisplayServicesCanChangeBrightness(CGDirectDisplayID id)
__attribute__((weak_import));

extern int
DisplayServicesGetBrightness(CGDirectDisplayID id, float *level)
__attribute__((weak_import));

extern int DisplayServicesSetBrightness(CGDirectDisplayID id, float level)
__attribute__((weak_import));

//
// Further candidates are:
//

//
// addPoints
// CreateDisplayIDInfoDictionary
// CreateGlobalID
// CreateStringFromOSType
// deBoor
// deviceGetLargeReport
// deviceGetReport
// deviceIDForDisplayID
// deviceRelease
// deviceSetLargeReport
// deviceSetReport
// DeviceValueFromAbstract
// displayDidReconfigure
// DisplayServicesAmbientLightCompensationEnabled
// DisplayServicesAmbientLightResetChanged
// DisplayServicesBezelButtonsLocked
// DisplayServicesBrightnessChanged
// DisplayServicesBrightnessChangeNotification
// DisplayServicesBrightnessChangeNotificationImmediate
// DisplayServicesCanChangeBrightness
// DisplayServicesCanResetAmbientLight
// DisplayServicesCommitSettings
// DisplayServicesCreateBrightnessTable
// DisplayServicesEnableAmbientLightCompensation
// DisplayServicesGetAuthorized
// DisplayServicesGetBrightness
// DisplayServicesGetBrightnessButtonsEnabled
// DisplayServicesGetBrightnessIncrement
// DisplayServicesGetCommitInterval
// DisplayServicesGetDevice
// DisplayServicesGetDeviceForDeviceID
// DisplayServicesGetDeviceID
// DisplayServicesGetDisplayID
// DisplayServicesGetDynamicSlider
// DisplayServicesGetLinearBrightness
// DisplayServicesGetLinearBrightnessUsableRange
// DisplayServicesGetPowerButtonEnabled
// DisplayServicesGetPowerMode
// DisplayServicesGetPowerSwitchMode
// DisplayServicesHasAmbientLightCompensation
// DisplayServicesHasBrightnessButtons
// DisplayServicesHasCommit
// DisplayServicesHasOptionsAuthorization
// DisplayServicesHasPowerButton
// DisplayServicesHasPowerMode
// DisplayServicesHasTouchSwitchDisable
// DisplayServicesIsBuiltInDisplay
// DisplayServicesIsSmartDisplay
// DisplayServicesNeedsBrightnessSmoothing
// DisplayServicesRegisterForAmbientLightCompensationNotifications
// DisplayServicesRegisterForAuthorizationNotifications
// DisplayServicesRegisterForBrightnessChangeNotifications
// DisplayServicesRegisterForDeviceTerminationNotification
// DisplayServicesRegisterForDisplayUSBHotplugNotifications
// DisplayServicesRegisterForNotification
// DisplayServicesRegisterForResetAmbientLightNotifications
// DisplayServicesResetAmbientLight
// DisplayServicesResetAmbientLightAll
// DisplayServicesSetAuthorized
// DisplayServicesSetBrightness
// DisplayServicesSetBrightnessButtonsEnabled
// DisplayServicesSetBrightnessSmooth
// DisplayServicesSetBrightnessWithType
// DisplayServicesSetDynamicSlider
// DisplayServicesSetLinearBrightness
// DisplayServicesSetPowerButtonEnabled
// DisplayServicesSetPowerMode
// DisplayServicesSetToDefaults
// DisplayServicesSetTouchSwitchModeForBit
// DisplayServicesTerminateDeviceID
// DisplayServicesUnregisterForAmbientLightCompensationNotifications
// DisplayServicesUnregisterForAuthorizationNotifications
// DisplayServicesUnregisterForBrightnessChangeNotifications
// DisplayServicesUnregisterForDeviceTerminationNotification
// DisplayServicesUnregisterForDisplayUSBHotplugNotifications
// DisplayServicesUnregisterForNotification
// DisplayServicesUnregisterForResetAmbientLightNotifications
// DSBrightnessCurveGeneratePoint
// DSBrightnessExpertCreateBrightnessTable
// DSBrightnessExpertGetBrightness
// DSBrightnessExpertGetBrightnessIncrement
// DSBrightnessExpertGetLinearBrightness
// DSBrightnessExpertGetUsableLinearRange
// DSBrightnessExpertNeedsTranslation
// DSBrightnessExpertNew
// DSBrightnessExpertRelease
// DSBrightnessExpertSetBrightness
// DSBrightnessExpertTransformBrightness
// DSBrightnessExpertTypeIsLinear
// DSCreateGlobalID
// DSCreateUsageRef
// DSDeviceAcquireDeviceLock
// DSDeviceBezelLocked
// DSDeviceBrightnessSmoothing
// DSDeviceBuiltInDisplayID
// DSDeviceCommitAll
// DSDeviceCommitSettings
// DSDeviceCreateReportsDictionary
// DSDeviceCreateSharedMemory
// DSDeviceDeviceID
// DSDeviceDeviceIDString
// DSDeviceDisableAmbientMonitoring
// DSDeviceDisplayIDForDeviceID
// DSDeviceEnableAmbientMonitoring
// DSDeviceEnableColorCorrection
// DSDeviceForDeviceID
// DSDeviceForDisplayID
// DSDeviceGetDynamicSlider
// DSDeviceGetFloatReport
// DSDeviceGetLargeReport
// DSDeviceGetLinearMappingArray
// DSDeviceGetReport
// DSDeviceGetReportBounds
// DSDeviceGetReportDictionary
// DSDeviceGetReportIncrement
// DSDeviceGetReportSize
// DSDeviceGetTemperature
// DSDeviceGetUsableRange
// DSDeviceHasReport
// DSDeviceIsEDM
// DSDeviceNew
// DSDeviceOnline
// DSDeviceProductID
// DSDeviceRationalReportValue
// DSDeviceRelease
// DSDeviceSerialNumber
// DSDeviceSessionConnected
// DSDeviceSetDynamicSlider
// DSDeviceSetFloatReport
// DSDeviceSetLargeReport
// DSDeviceSetReport
// DSDeviceSetToDefaults
// DSDeviceTerminateDeviceID
// DSDeviceVendorID
// DSDeviceVersion
// DSGetCGSService
// DSGetFeatureDict
// DSHIDDeviceBezelLocked
// DSHIDDeviceCommit
// DSHIDDeviceCreateGestaltTable
// DSHIDDeviceCreateLADTable
// DSHIDDeviceDeviceID
// DSHIDDeviceDisableAmbientMonitoring
// DSHIDDeviceEnableAmbientMonitoring
// DSHIDDeviceEnableColorCorrection
// DSHIDDeviceGetLargeReport
// DSHIDDeviceGetReport
// DSHIDDeviceGetTemperature
// DSHIDDeviceNew
// DSHIDDeviceNewExt
// DSHIDDeviceProductID
// DSHIDDeviceRelease
// DSHIDDeviceReleaseExt
// DSHIDDeviceSerialNumber
// DSHIDDeviceSetLargeReport
// DSHIDDeviceSetReport
// DSHIDDeviceVendorID
// DSHIDDeviceVersion
// DSIICDeviceCommit
// DSIICDeviceEnableColorCorrection
// DSIICDeviceEnableMCCSBrightnessNotifications
// DSIICDeviceFidelity
// DSIICDeviceGetDynamicSlider
// DSIICDeviceGetLargeReport
// DSIICDeviceGetReport
// DSIICDeviceGetTemperature
// DSIICDeviceIsEDM
// DSIICDeviceNew
// DSIICDeviceProductID
// DSIICDeviceRefreshIODisplayService
// DSIICDeviceRelease
// DSIICDeviceSerialNumber
// DSIICDeviceSetDynamicSlider
// DSIICDeviceSetLargeReport
// DSIICDeviceSetReport
// DSIICDeviceSetToDefaults
// DSIICDeviceVendorID
// DSIICDeviceVersion
// DSLogClose
// DSMachSeconds
// findInterval
// findY
// getDisplayInfo
// getReportBoundsFromDict
// getReportIDFromDict
// getReportSizeFromDict
// handleTemperatureChange
// HexStringToNum
// makeDeviceID
// markDeviceTerminated
// newPoint
// O3Delay
// OSTypeFromString
// perceptualToPhoton
// perceptualToPhotonCIE
// perceptualToPhotonLog
// photonToPerceptual
// photonToPerceptualCIE
// photonToPerceptualLog
// RationalizedAbstractValue
// registerSleepWakeNotifications
// scalePoint
// scaleToAbstract
// scaleToDevice
// setGammaDictionary
// unregisterSleepWakeNotifications


#endif 
