//
//  PullusBase.h
//  PullusBase
//
//  Created by Frank Schuster on 12.12.22.
//  Copyright © 2022 Frank Schuster. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for PullusBase.
FOUNDATION_EXPORT double PullusBaseVersionNumber;

//! Project version string for PullusBase.
FOUNDATION_EXPORT const unsigned char PullusBaseVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PullusBase/PublicHeader.h>


