public
extension Comparable {
	
	///
	/// Returns self, if it is less than `value`, otherwise `value`.
	///
	/// Same as `min(self, value)`.
	///
	public
	func atMost(_ value: Self) -> Self {
		return Swift.min(self, value)
	}
	
}
