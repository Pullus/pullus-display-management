public
extension Comparable {
	
	///
	/// Returns self, if it is greater than `value`, otherwise `value`.
	///
	/// Same as `max(self, value)`.
	///
	public
	func atLeast(_ value: Self) -> Self {
		return Swift.max(self, value)
	}
	
}
