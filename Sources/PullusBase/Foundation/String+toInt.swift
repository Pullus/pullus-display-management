import Swift

public
extension String {
	
	///
	/// Convenience function for chaining.
	///
	public
	func toInt() -> Int? {
		return Int(self)
	}
	
}
