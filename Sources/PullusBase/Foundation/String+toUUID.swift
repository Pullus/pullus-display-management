import Foundation

public
extension String {
	
	///
	/// Convenience function for chaining.
	///
	public
	func toUUID() -> UUID? {
		return UUID(uuidString: self)
	}
	
}
