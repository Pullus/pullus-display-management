import Swift

public
extension String {
	
	///
	/// Returns a description or a default value if the value is missing.
	///
	public
	init<VALUE>(describing aValue: VALUE?, default: String) {
		if let value = aValue {
			self.init(describing: value)
		} else {
			self.init(`default`)
		}
	}
	
}
