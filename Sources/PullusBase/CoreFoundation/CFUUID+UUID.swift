import Foundation

public
extension CFUUID {
	
	///
	/// This Core Foundation `CFUUID` as an `UUID`.
	///
	public
	var UUID: UUID {
		var bytes: CFUUIDBytes = CFUUIDGetUUIDBytes(self)
		
		return withUnsafePointer(to: &bytes) {
			$0.withMemoryRebound(to: UInt8.self, capacity: 16) {
				NSUUID(uuidBytes: $0) as UUID }
		}
	}
	
}
