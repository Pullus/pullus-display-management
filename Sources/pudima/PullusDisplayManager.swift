import Foundation

import PullusDisplayManagement

fileprivate
extension Int {
	
	///
	/// Checks if this integer is contained in an interval and returns this
	/// value.
	///
	/// If this value is outside the interval,  an exception is raised with the
	/// specified message.
	///
	func checked(in range: ClosedRange<Int>, onErrorThrow message: String) throws -> Int {
		guard range.contains(self) else { throw Failure(message) }
		
		return self
	}
	
}

fileprivate
extension Bool {
	
	///
	/// Returns "yes" or "no".
	///
	func toString() -> String {
		return self ? "yes" : "no"
	}
	
}

fileprivate
extension CGDirectDisplayID {
	
	///
	/// Returns a report with some properties.
	///
	func report() -> String {
		return [
			String(format: "           ID: 0x%08x", self),
			String(format: "         UUID: %@", UUID.uuidString),
			serialNumber.map { String(format: "serial number: 0x%08x", $0) },
			vendorNumber.map { String(format: "vendor number: 0x%08x", $0) },
			String(format: "         main: %@", isMain.toString()),
			String(format: "   brightness: %@ %%", String(describing: getBrightnessAsPercent(), default: "-")),
			String(format: "       active: %@", isActive.toString()),
			String(format: "       asleep: %@", isAsleep.toString()),
			String(format: "      builtin: %@", isBuiltin.toString()),
			String(format: "       online: %@", isOnline.toString()),
			String(format: "  pyhsic size: %0.f mm x %0.f mm", size.width, size.height),
			String(format: "   resolution: %.0f pt x %.0f pt", resolution.width, resolution.height),
			mode.map { String(format: "         size: %d px x %d px", $0.pixelWidth, $0.pixelHeight) },
			mode.map { String(format: " refresh rate: %.0f Hz", $0.refreshRate) },
			String(format: "       stereo: %@", isStereo.toString())
			].flatMap { $0 }.joined(separator: "\n")
	}
	
}

///
/// The main class of the `pudima` command line tool.
///
/// The implementation uses the `UUID`s and not the `ID`s of the displays for
/// identification because
///  - Display ID's can change after reboot. "… A display ID can persist across
///    processes and typically remains constant until the machine is restarted.
///    …"
///  - Systems with multiple GPU's and auto-switching have a display ID for
///    each GPU but the UUID is the same. E.g. MacBook Pro's mid-2010.
///
/// To avoid any dependency on a command line framework, the evaluation of
/// command line arguments is done by hand -- using a (recursive) descent
/// parser.
///
class PullusDisplayManager {
	
	///
	/// The input arguments used as input queue.
	///
	private
	var arguments: ArraySlice<String> = []
	
	///
	/// Tries to read the first argument from `arguments` and tries to "convert"
	/// it to a string.
	///
	/// If the first argument is missing (`arguments` is empty) the specified
	/// exception is raised.
	///
	/// The first argument is removed from the `arguments`.
	///
	private
	func parseString(onErrorThrow message: String) throws -> String {
		guard let value = arguments.popFirst() else { throw Failure(message) }
		
		return value
	}
	
	///
	/// Tries to read the first argument from `arguments` and tries to convert
	/// it to an `UUID`.
	///
	/// If the first argument is missing (`arguments` is empty) the specified
	/// exception is raised.
	///
	/// The first argument is removed from the `arguments`.
	///
	private
	func parseUUID(onErrorThrow message: String) throws -> UUID {
		guard
			let value = arguments.popFirst()?.toUUID()
			else { throw Failure(message) }
		
		return value
	}
	
	///
	/// Tries to read the first argument from `arguments` and tries to convert
	/// it to an integer.
	///
	/// If the first argument is missing (`arguments` is empty) the specified
	/// exception is raised.
	///
	/// The first argument is removed from the `arguments`.
	///
	private
	func parseInt(onErrorThrow message: String) throws -> Int {
		guard
			let value = arguments.popFirst()?.toInt()
			else { throw Failure(message) }
		
		return value
	}
	
	// MARK: -
	
	///
	/// Starts the parsing of the arguments and executes the commands.
	///
	func run(arguments anArguments: [String]) -> Bool {
		do {
			// Drop the name of the executable (pudima)
			arguments = anArguments.dropFirst()
			
			try parseCommand()
			
			guard
				arguments.isEmpty
				else { throw Failure("ignored supernumerary arguments") }
			
			return true
		} catch {
			printUsage(String(describing: error))
			return false
		}
	}
	
	private
	func printUsage(_ message: String? = nil) {
		print("NAME")
		print("    pudima - Pullus Display Manager")
		print()
		print("SYNOPSIS")
		print("    Manage connected (Apple) displays.")
		print()
		print("    help")
		print("        Shows this text.")
		print()
		print("    main")
		print("        Shows the main display.")
		print()
		print("    list")
		print("        Shows all displays.")
		print()
		print("    brightness set <level>")
		print("        Sets the brightness of all displays to a level (0 to 100).")
		print()
		print("    brightness change <delta>")
		print("        Changes the brightness of all displays by a delta (-100 to 100).")
		print()
		print("    display <uuid> brightness set <level>")
		print("        Sets the brightness of a specific display (hex number) to a level (0 to 100).")
		print()
		print("    display <uuid> brightness change <delta>")
		print("        Changes the brightness of a specific display (UUID) by a delta (-100 to 100).")
		print()
		print("EXAMPLES")
		print("    pudima brightness set 100")
		print()
		print("    Sets all displays to the maximum brightness.")
		print()
		print("AUTHOR")
		print("    Frank Schuster")
		
		if let message = message {
			print()
			print("Failure:", message)
		}
	}
	
	///
	/// ```
	/// <command> ::= "help"
	///             | "list"
	///             | "main"
	///             | "brightness" "set" <level>
	///             | "brightness" "change" <delta>
	///             | "display" <uuid> "brightness" "set" <level>
	///             | "display" <uuid> "brightness" "change" <delta>
	///             .
	///    <uuid> ::= »An UUID of a display« .
	///   <level> ::= »An integer number from 0 to 100« .
	///   <delta> ::= »An integer number from -100 to 100« .
	/// ```
	private
	func parseCommand() throws {
		switch try parseString(onErrorThrow: "Command is missing.") {
		case "help":		printUsage()
		case "list":		try parseList()
		case "main":		try parseMain()
		case "brightness":	try parseBrightness()
		case "display":		try parseDisplay()
		default:			throw Failure("Unknown command")
		}
	}
	
	private
	func parseList() throws {
		CGDirectDisplayID.displays
			.sorted { a, b in a.UUID.uuidString < b.UUID.uuidString }
			.enumerated()
			.forEach { index, display in
				print("      Display:", index)
				print(display.report())
		}
	}
	
	private
	func parseMain() throws {
		print(CGMainDisplayID().report())
	}
	
	private
	func parseBrightness() throws {
		switch try parseString(onErrorThrow: "Can't read subcommand.") {
		case "change":	try parseBrightnessChange()
		case "set":		try parseBrightnessSet()
		default:		throw Failure("Can't read subcommand")
		}
	}
	
	private
	func parseBrightnessSet() throws {
		let level = try parseInt(onErrorThrow: "Can't read brightness level.")
			.checked(in: 0...100, onErrorThrow: "Can't use the brightness level. Range is 0...100.")
		
		CGDirectDisplayID.displays.forEach { display in
			display.setBrightness(toPercent: level)
		}
	}
	
	private
	func parseBrightnessChange() throws {
		let delta = try parseInt(onErrorThrow: "Can't read delta.")
			.checked(in: -100...100, onErrorThrow: "Can't use delta. Range is -100...100.")
		
		CGDirectDisplayID.displays.forEach { display in
			display.changeBrightness(byPercent: delta)
		}
	}
	
	private
	func parseDisplay() throws {
		let displayUUID = try parseUUID(onErrorThrow: "Can't read the display UUID.")
		
		// IMPORTANT: The display (id) can only be used after the displays (ids)
		//            are fetched (e.g. with `DirectDisplay.displays`)
		guard
			let display = CGDirectDisplayID.displays.first(where: { $0.UUID == displayUUID } )
			else { throw Failure("Unknown display number") }
		
		try parseDisplay(display)
	}
	
	private
	func parseDisplay(_ display: CGDirectDisplayID) throws {
		switch try parseString(onErrorThrow: "Can't read the display subcommand.") {
		case "brightness":	try parseDisplayBrightness(display)
		default:			throw Failure("Unknown subcommand.")
		}
	}
	
	private
	func parseDisplayBrightness(_ display: CGDirectDisplayID) throws {
		switch try parseString(onErrorThrow: "Can't read subcommand.") {
		case "change":	try parseDisplayBrightnessChange(display)
		case "set":		try parseDisplayBrightnessSet(display)
		default:		throw Failure("Can't read subcommand.")
		}
	}
	
	private
	func parseDisplayBrightnessSet(_ display: CGDirectDisplayID) throws {
		let level = try parseInt(onErrorThrow: "Can't read the brightness level.")
			.checked(in: 0...100, onErrorThrow: "Can't use the brightness level. Range is 0...100.")
		
		display.setBrightness(toPercent: level)
	}
	
	private
	func parseDisplayBrightnessChange(_ display: CGDirectDisplayID) throws {
		let delta = try parseInt(onErrorThrow: "Can't read delta.")
			.checked(in: -100...100, onErrorThrow: "Can't use delta. Range is -100...100.")
		
		display.changeBrightness(byPercent: delta)
	}
	
}
