import Foundation

///
/// Type for all parse and execution errors.
///
public
struct Failure : Error, CustomStringConvertible {

	///
	/// Returns the error message.
	///
	public
	var description: String { return message }
	
	///
	/// An error message.
	///
	private
	let message: String
	
	public
	init(_ aMessage: String) {
		message = aMessage
	}
	
}
