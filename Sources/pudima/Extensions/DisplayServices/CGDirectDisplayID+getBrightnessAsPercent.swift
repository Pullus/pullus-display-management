import PullusDisplayManagement

public
extension CGDirectDisplayID {
	
	///
	/// Returns the brightness (in percent) of this display.
	///
	public
	func getBrightnessAsPercent() -> Int? {
		return getBrightness().map { $0 * 100 }.map { Int($0) }
	}
	
}
