import PullusDisplayManagement

public
extension CGDirectDisplayID {
	
	///
	/// Changes the brightness (in percent of the maximum) of this display.
	///
	public
	func changeBrightness(byPercent aDelta: Int) {
		guard let level = getBrightness() else { return }
		
		setBrightness(to: level + Float(aDelta) / 100.0)
	}
	
}
