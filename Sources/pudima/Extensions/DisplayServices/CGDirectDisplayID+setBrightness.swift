import PullusDisplayManagement

public
extension CGDirectDisplayID {
	
	///
	/// Sets the brightness (in percent) of this display.
	///
	public
	func setBrightness(toPercent aLevel: Int) {
		setBrightness(to: Float(aLevel) / 100.0)
	}
	
}
