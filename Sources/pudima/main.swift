import Foundation

let ok = PullusDisplayManager().run(arguments: CommandLine.arguments)

exit(ok ? 0 : 1)
